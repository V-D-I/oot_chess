package com.epam.object_oriented_thinking.system;

import com.epam.object_oriented_thinking.entity.table.Table;

import java.util.List;

public class Game {
    private static Game instance;
    private Table table;
    private List<Player> players;
    private Player activePlayer;

    private Game() {
    }

    public static Game getInstance() {

        // Some VERY BAD realisation of singleton
        if (instance == null) {
            instance = new Game();
        }
        return instance;
    }

    public void startGame() {

    }
    public void finishGame() {

    }
}
