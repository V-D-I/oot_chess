package com.epam.object_oriented_thinking.entity;

public interface Drawable {
    void draw();
}
