package com.epam.object_oriented_thinking.entity.figures;

import com.epam.object_oriented_thinking.entity.Drawable;
import com.epam.object_oriented_thinking.entity.table.Cell;
import com.epam.object_oriented_thinking.utils.Vector;

public abstract class Figure implements Drawable {
    protected String name;
    protected int teamId;
    protected boolean isDead;
    protected Cell tookCell;

    public Figure(int teamId, Cell tookCell) {
        this.teamId = teamId;
        this.tookCell = tookCell;
    }

    public void tryToMove(Vector position) {

    }

    public boolean canMoveTo(Vector position) {
        return false;
    }

    private void moveTo(Vector position) {

    }

    public void onDead() {
    }

    public void onMoved() {
    }

    @Override
    public void draw() {

    }
}
