package com.epam.object_oriented_thinking.entity.figures;

import com.epam.object_oriented_thinking.entity.table.Cell;
import com.epam.object_oriented_thinking.utils.Vector;


public class Rook extends Figure {

    public Rook(int teamId, Cell tookCell) {
        super(teamId, tookCell);
        this.name = "Rook";
    }

    @Override
    public boolean canMoveTo(Vector position) {
        //Some implementation
        return false;
    }

    @Override
    public void draw() {

    }
}
