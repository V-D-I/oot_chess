package com.epam.object_oriented_thinking.entity.figures;

import com.epam.object_oriented_thinking.entity.table.Cell;
import com.epam.object_oriented_thinking.system.Game;
import com.epam.object_oriented_thinking.utils.Vector;


public class King extends Figure {

    public King(int teamId, Cell tookCell) {
        super(teamId, tookCell);
        this.name = "King";
    }

    @Override
    public boolean canMoveTo(Vector position) {
        //Some implementation
        return false;
    }

    @Override
    public void onDead() {

    }

    @Override
    public void draw() {

    }
}
