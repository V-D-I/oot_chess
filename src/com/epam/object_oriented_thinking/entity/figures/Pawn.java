package com.epam.object_oriented_thinking.entity.figures;

import com.epam.object_oriented_thinking.entity.table.Cell;
import com.epam.object_oriented_thinking.utils.Vector;


public class Pawn extends Figure {

    public Pawn(int teamId, Cell tookCell) {
        super(teamId, tookCell);
        this.name = "Pawn";
    }

    @Override
    public boolean canMoveTo(Vector position) {
        //Some implementation
        return false;
    }

    @Override
    public void onMoved() {
        super.onMoved();
    }

    @Override
    public void draw() {

    }
}
