package com.epam.object_oriented_thinking.entity.table;

import com.epam.object_oriented_thinking.entity.Drawable;
import com.epam.object_oriented_thinking.entity.figures.Figure;
import com.epam.object_oriented_thinking.utils.Vector;

import java.awt.*;

public class Cell implements Drawable {
    private Vector position;
    private Figure occupiedBy;
    private Color color;

    @Override
    public void draw() {

    }

    public boolean isFree() {
        return occupiedBy == null;
    }
}
